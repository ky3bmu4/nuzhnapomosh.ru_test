//------------------------------------
//  Form
//------------------------------------

$.validate({
  errorElementClass: 'input__field_error',
  borderColorOnError: '',
  errorMessageClass: 'input__error',
  onSuccess: function ($form) {
    $.ajax({
      url: $form.attr('action'),
      type: $form.attr('method'),
      data: {
        second_name: $('#input-second-name').val(),
        first_name: $('#input-first-name').val(),
        last_name: $('#input-last-name').val(),
        email: $('#input-email').val(),
        phone: $('#input-phone').val(),
        message: $('#input-message').val()
      },
      success: function (response) {
        $(".modal__container-form").css('display', 'none')
        $(".modal__container-thanks").css('display', 'block')
      }
    })
    return false;
  }
});

//------------------------------------
//  Modal
//------------------------------------

var openModal = function(wrapper) {
  $('body')
    .addClass('modal-open')

  $(wrapper)
    .addClass('show');
}

var closeModal = function(wrapper) {
  $('.modal__overlay, .modal__close').on('click', function (e) {
    $(wrapper)
      .removeClass('show')

    $('body').removeClass('modal-open')
  })
  $(document).keyup(function (e) {
    if (e.keyCode === 27) {
      $(wrapper)
        .removeClass('show')

      $('body')
        .removeClass('modal-open')
    }
  });
}


//------------------------------------
//  Modal
//------------------------------------

$('.button_content').on('click', function (e) {
  openModal('.modal');
  return false;
});
closeModal('.modal');


//------------------------------------
//  Mask
//------------------------------------

$(function() {
  $("#input-phone").mask("+7 (999) 999 99-99", { placeholder: " " });
});