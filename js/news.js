//------------------------------------
//  News list
//------------------------------------

var newsList = [
  {
    id: 0,
    title: 'Выходящий принцип восприятия: основные моменты',
    date: 1512932400000,
    image: 'images/news/1.jpg',
    href: '#'
  },
  {
    id: 1,
    title: 'Из ряда вон выходящий принцип восприятия: основные моменты',
    date: 1513278000000,
    image: 'images/news/4.jpg',
    href: '#'
  },
  {
    id: 2,
    title: 'Принцип восприятия: основные моменты',
    date: 1513191600000,
    image: 'images/news/3.jpg',
    href: '#'
  },
  {
    id: 3,
    title: 'Основные моменты',
    date: 1513018800000,
    image: 'images/news/2.jpg',
    href: '#'
  }
]

var dateOptions = { weekday: 'numeric', year: 'numeric', month: 'numeric', day: 'numeric' };
var createList = function (array) {
  var template = ''
  array.forEach(function (item) {
    var newsDate = new Date(item.date)
    newsDate = ('0' + newsDate.getDate()).slice(-2) + '.' + ('0' + (newsDate.getMonth() + 1)).slice(-2) + '.' + newsDate.getFullYear()

    template += '<div class="news clearfix"><a href="' + item.href + '" title="' + item.title + '" class="news__image"><img src="' + item.image + '" width="130" height="80" alt="' + item.title + '" /></a><div class="news__content"><a href="' + item.href + '" title="' + item.title + '" class="link news__text">' + item.title + '</a><span class="news__date">' + newsDate + '</span></div></div>'
  })
  document.getElementsByClassName('news-list')[0].innerHTML = template
}
createList(newsList)
var linkNew = document.getElementsByClassName('aside__title_new')[0]
var linkPop = document.getElementsByClassName('aside__title_pop')[0]

linkNew.onclick = function () {
  newsList.sort(function (a, b) {
    if (a.date < b.date) {
      return 1
    }
    if (a.date > b.date) {
      return -1
    }
    return 0
  })
  createList(newsList)
  linkNew.style.display = 'none'
  linkPop.style.display = 'block'
}

linkPop.onclick = function () {
  newsList.sort(function (a, b) {
    if (a.id > b.id) {
      return 1
    }
    if (a.id < b.id) {
      return -1
    }
    return 0
  })
  createList(newsList)
  linkNew.style.display = ''
  linkPop.style.display = ''
}
